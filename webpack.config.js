const path = require("path");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const HtmlWebPackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const dist = path.resolve(__dirname, "dist");

module.exports = {
    "entry": {
        "main": "./src/"
    },
    "output": {
        "filename": "[name].bundle.js",
        "chunkFilename": "[name].bundle.js",
        "path": dist
    },
    "mode": "development",
    "module": {
        "rules": [
            {
                "test": /\.(js|jsx)$/,
                "exclude": /node_modules/,
                "use": {
                    "loader": "babel-loader"
                }
            },
            {
                "test": /\.html$/,
                "use": [
                    {
                        "loader": "html-loader",
                        "options": {"minimize": true}
                    }
                ]
            },
            {
                "test": /\.(png|jpe?g|bmp|gif)/i,
                "use": [
                    {
                        "loader": "url-loader",
                        "options": {
                            "name": "./img/[name].[ext]",
                            "limit": 10000
                        }
                    },
                    {
                        "loader": "img-loader"
                    }
                ]
            },
            {
                "test": /\.scss$/,
                "use": [
                    MiniCssExtractPlugin.loader,
                    "css-loader",
                    {
                        "loader": "postcss-loader",
                        "options": {"plugins": [require("autoprefixer")]}
                    },
                    "sass-loader"
                ]
            },
            {
                "test": /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                "use": [
                    {
                        "loader": "url-loader",
                        "options": {
                            "limit": 1000,
                            "mimetype": "application/font-woff"
                        }
                    }
                ]
            },
            {
                "test": /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                "loader": "file-loader"
            }
        ]
    },
    "resolve": {
        "extensions": [".js", ".json", ".jsx", ".css", ".scss"]
    },
    "devtool": "source-map",
    "cache": false,
    "watchOptions": {
        "aggregateTimeout": 100
    },
    "devServer": {
        "contentBase": path.join(__dirname, 'dist'),
        "port": 9000
    },
    "plugins": [
        new CleanWebpackPlugin(dist, {}),
        new HtmlWebPackPlugin({
            "inject": true,
            "hash": true,
            "template": "./src/index.html",
            "filename": "index.html"
        }),
        new MiniCssExtractPlugin({
            "filename": "[name].css",
            "chunkFilename": "[id].css"
        })
    ]
};

