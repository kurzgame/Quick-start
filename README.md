# Quick start

Just a basic configuration user for webpack-4 + react + redux + scss + saga

Ready to be covered by tests using "jest" framework

## How to use

 - ```npm install``` - to install all required dependencies
 - ```npm run test``` - to run "jest" tests
 - ```npm run build:prod``` - to make a production-ready bundle

