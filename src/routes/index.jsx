import React from "react";
import { Route, Switch } from "react-router";
import Home from "../containers/Home";
import About from "../containers/About";
import Contacts from "../containers/Contacts";

export default (
    <React.Fragment>
        <Switch>
            <Route exact path="/" component={Home} />
            <Route path="/about" component={About} />
            <Route path="/contacts" component={Contacts} />
        </Switch>
    </React.Fragment>
);
