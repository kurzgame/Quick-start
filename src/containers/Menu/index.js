import { connect } from "react-redux";
import Menu from "./Menu";

import { login } from "../../actions";


const mapStateToProps = state => ({
    "user": state.user
});

const mapDispatchToProps = dispatch => ({
    "login": data => dispatch(login(data))
});

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
