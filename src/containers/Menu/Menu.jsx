import React from "react";
import PropTypes from "prop-types";

import "./style.scss";


class Menu extends React.PureComponent {
    login = () => {
        this.props.login({});
    };

    render () {
        const { user } = this.props;

        return <header className={`container ${this.props.className}`}>
            <section>
                <h1>Example App</h1>
            </section>
            <section>
                <span className="user-info">{user.info.name}</span>
                <button className="btn" onClick={this.login}>Log In</button>
            </section>
        </header>
    }
}

Menu.propTypes = {
    user: PropTypes.object.isRequired,
    login: PropTypes.func.isRequired
};


export default Menu;
