import React from "react";
import Menu from "./Menu";
import renderer from "react-test-renderer";

describe("Menu container", () => {
    test("login", () => {
        const userData = { "info": { "name": "John Doe" } };
        const onClick = jest.fn();
        const component = renderer.create(<Menu user={userData} login={onClick} />);
        const button = component.root.findByProps({ className: "btn" });
        const userInfo = component.root.findByProps({ className: "user-info" });
        const tree = component.toJSON();

        expect(button).toBeDefined();
        expect(userInfo).toBeDefined();

        expect(tree).toMatchSnapshot();

        expect(userInfo.children).toEqual(["John Doe"]);

        button.props.onClick({ preventDefault: jest.fn() });

        expect(onClick).toHaveBeenCalledTimes(1);
        expect(onClick).toHaveBeenCalledWith({});
    });
});
