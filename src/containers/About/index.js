import { connect } from "react-redux";
import { push } from "connected-react-router";
import About from "./About";

export default connect(null, { push })(About);
