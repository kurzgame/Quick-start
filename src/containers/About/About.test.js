import React from "react";
import About from "./About";
import renderer from "react-test-renderer";

describe("About container", () => {
    test("go to main route", () => {
        const onClick = jest.fn();
        const component = renderer.create(<About push={onClick} />);
        const button = component.root.findByProps({ className: "btn" });
        const tree = component.toJSON();

        expect(button).toBeDefined();

        expect(tree).toMatchSnapshot();

        button.props.onClick({ preventDefault: jest.fn() });

        expect(onClick).toHaveBeenCalledTimes(1);
        expect(onClick).toHaveBeenCalledWith("/");
    });
});
