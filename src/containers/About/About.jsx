import React from "react";
import PropTypes from "prop-types";

import Title from "../../components/Title";
import "./style.scss";


class About extends React.PureComponent {
    goHome = () => {
        this.props.push("/");
    };

    render () {
        return <article className={`container ${this.props.className}`}>
            <Title text="About" />
            <section>
                <p>For learning & further re-use purposes.</p>
                <p>Build on top of:</p>
                <ul>
                    <li>React</li>
                    <li>Redux</li>
                    <li>React-Router</li>
                    <li>Redux-Saga</li>
                    <li>React-Modal</li>
                </ul>
                <button className="btn" onClick={this.goHome}>Go Home!</button>
            </section>
        </article>
    }
}

About.propTypes = {
    push: PropTypes.func.isRequired
};

export default About;
