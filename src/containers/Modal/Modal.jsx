import React from "react";
import Modal from "react-modal";
import Title from "../../components/Title";


class MyModal extends React.Component {
    static ERROR_STYLE = {
        "color": "red"
    };

    render () {
        const errorMessage = this.props.error;
        return <Modal isOpen={Boolean(errorMessage)}
                      onRequestClose={this.props.passError}
                      style={MyModal.ERROR_STYLE}
                      contentLabel={errorMessage} >
            <Title text="Error" />
            <p className="text">{errorMessage}</p>
            <button className="btn" onClick={this.props.passError}>close</button>
        </Modal>
    }
}

export default MyModal;
