import { connect } from "react-redux";
import Modal from "./Modal";
import { passError } from "../../actions";


const mapStateToProps = state => ({
    "error": state.error
});

const mapDispatchToProps = dispatch => ({
    "passError": () => dispatch(passError())
});

export default connect(mapStateToProps, mapDispatchToProps)(Modal);
