import { connect } from "react-redux";
import { push } from "connected-react-router";
import Contacts from "./Contacts";

export default connect(null, { push })(Contacts);
