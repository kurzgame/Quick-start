import React from "react";
import PropTypes from "prop-types";

import Title from "../../components/Title";
import "./style.scss";

import packageJSON from "../../../package.json";


class Contacts extends React.PureComponent {
    goHome = () => {
        this.props.push("/");
    };

    render () {
        const mail = packageJSON.author;
        return <article className={`container ${this.props.className}`}>
            <Title text="Contacts" />
            <address>
                <p>Feel free to contact me at:</p>
                <a href={`mailto:${mail}`}>{mail}</a>
            </address>
            <button className="btn" onClick={this.goHome}>Go Home!</button>
        </article>
    }
}

Contacts.propTypes = {
    push: PropTypes.func.isRequired
};

export default Contacts;
