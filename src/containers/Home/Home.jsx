import React from "react";
import PropTypes from "prop-types";
import {Link} from "react-router-dom";

import Title from "../../components/Title";
import "./style.scss";


class Home extends React.PureComponent {
    render () {
        return <header className={`container-greeting ${this.props.className}`}>
            <section>
                <Title text="Home"/>
            </section>
            <section>
                <ul>
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/about">About</Link></li>
                    <li><Link to="/contacts">Contacts</Link></li>
                </ul>
            </section>
            <section>
                <p>
                    Greeting, sir!
                </p>
            </section>
        </header>
    }
}

Home.propTypes = {};

export default Home;
