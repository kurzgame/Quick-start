import { login as loginTypes } from "../constants";

export const login = (data) => ({
    "type": loginTypes.USER_LOGIN, "payload": data
});
