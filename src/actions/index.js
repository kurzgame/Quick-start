import { login } from "./login";
import { passError } from "./error";

export {
    login,
    passError
};
