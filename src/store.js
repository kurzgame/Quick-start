import { createBrowserHistory } from "history";
import { applyMiddleware, compose, createStore } from "redux";
import { routerMiddleware } from "connected-react-router";
import createSagaMiddleware from "redux-saga";

import createRootReducer from "./reducers";
import saga from "./sagas";

export const history = createBrowserHistory();
const sagaMiddleware = createSagaMiddleware();

const configureStore = (defaultState) => {
    return createStore(
        createRootReducer(history), // root reducer with router state
        defaultState,
        compose(
            applyMiddleware(
                sagaMiddleware,
                routerMiddleware(history) // for dispatching history actions
            ),
        ),
    );
};

const store = configureStore();

sagaMiddleware.run(saga);

export default store;
