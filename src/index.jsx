import { Provider } from "react-redux";
import React from "react";
import ReactDOM from "react-dom";
import Modal from "react-modal";

import App from "./App";
import store, { history } from "./store";

const root = document.querySelector("#root");
Modal.setAppElement(root);

ReactDOM.render(
    <React.Fragment>
        <Provider store={store}>
            <App history={history} />
        </Provider>
    </React.Fragment>,
    root);
