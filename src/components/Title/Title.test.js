import React from "react";
import Title from "./index";
import renderer from "react-test-renderer";

describe("Title component", () => {
    test("check for correctness", () => {
        const component = renderer.create(<Title className="test" text="Hello!" />);
        const tree = component.toJSON();

        expect(tree).toMatchSnapshot();

        expect(component.root.findByProps({ className: "component-title test" }).children).toEqual(["Hello!"]);
    });
});
