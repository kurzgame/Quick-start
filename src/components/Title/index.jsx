import React from "react";
import PropTypes from "prop-types";

const Title = ({ className="", text }) => (
    <h2 className={`component-title ${className}`}>{text}</h2>
);

Title.propTypes = {
    text: PropTypes.string.isRequired
};

export default Title;
