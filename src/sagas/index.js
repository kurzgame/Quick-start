import { takeEvery } from "redux-saga/effects"
import { login as loginTypes } from "../constants";
import login from "./login";


function* saga() {
    yield takeEvery(loginTypes.USER_LOGIN, login);
}

export default saga;
