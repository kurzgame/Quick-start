import { delay, call, put } from "redux-saga/effects";
import { login as loginTypes } from "../constants";

function* fetchUser(action) {
    yield delay(1000);
    // Don't even try...
    // yield put({ type: loginTypes.USER_LOGIN_SUCCEEDED, info: {...} });
    yield put({ type: loginTypes.USER_LOGIN_FAILED, message: "Oops! You are not registerd, what are you want from me then?" });
}

export default fetchUser;
