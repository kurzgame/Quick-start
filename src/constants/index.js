import login from "./login";
import error from "./error";

export {
    login,
    error
};
