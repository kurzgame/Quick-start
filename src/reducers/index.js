import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import user from "./user";
import error from "./error";


export default (history) => combineReducers({
    "router": connectRouter(history),
    user,
    error
});
