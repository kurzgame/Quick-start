import { login as loginTypes } from "../constants";

const defaultState = {
    "info": {
        "name": "Unknown",
        "age": 0
    },
    "connected": false
};

const user = (state=defaultState, action) => {
    switch (action.type) {
        case loginTypes.USER_LOGIN_SUCCESS:
            return {
                ...state,
                "info": action.info,
                "connected": true
            };
        default:
            return state;
    }
};

export default user;
