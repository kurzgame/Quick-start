import { login, error as errorTypes } from "../constants";


const error = (state="", action) => {
    switch (action.type) {
        case login.USER_LOGIN_FAILED:
            return action.message;
        case errorTypes.PASS_ERROR:
            return "";
        default:
            return state;
    }
};

export default error;
