import React from "react";
import PropTypes from "prop-types";
import { ConnectedRouter } from "connected-react-router";
import Modal from "./containers/Modal";

import routes from "./routes";
import Menu from "./containers/Menu";

import "./style.scss";


const App = ({ history }) => (
    <React.Fragment>
        <Modal />
        <ConnectedRouter history={history}>
            <Menu />
            { routes }
        </ConnectedRouter>
    </React.Fragment>
);

App.propTypes = {
    history: PropTypes.object.isRequired
};

export default App;

